<?php

namespace Drupal\video_embed_html5\Plugin\video_embed_field\Provider;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\Entity\File;
use Drupal\video_embed_field\ProviderPluginBase;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\FFMpeg;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Html5 provider plugin.
 *
 * @VideoEmbedProvider(
 *   id = "html_5",
 *   title = @Translation("HTML5")
 * )
 */
class Html5 extends ProviderPluginBase {

  /**
   * The video_embed_field config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * The video Url.
   *
   * @var string
   */
  protected string $videoUrl = '';

  /**
   * The File name.
   *
   * @var string
   */
  protected string $filename = '';

  /**
   * The Video type.
   *
   * @var string
   */
  protected string $videoType = '';

  /**
   * Html5 constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The id of the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The Guzzle HTTP client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Extension\ModuleExtensionList $moduleExtensionList
   *   The module extension list service.
   * @param \FFMpeg\FFMpeg|null $phpFfmpeg
   *   The optional PHP FFMPEG service.
   *
   * @throws \Exception
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    $plugin_definition,
    ClientInterface $http_client,
    ConfigFactoryInterface $config_factory,
    protected ModuleExtensionList $moduleExtensionList,
    protected ?FFMpeg $phpFfmpeg,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $http_client);
    $this->config = $config_factory->get('video_embed_html5.config');

    // Set filename for thumbnail.
    [$video_url, $video_type] = $this->getVideoId();
    $this->videoUrl = $video_url;
    $this->videoType = $video_type;
    $this->filename = md5($video_url);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $phpFfmpeg = NULL;
    $module_handler = $container->get('module_handler');
    if ($module_handler->moduleExists('php_ffmpeg')) {
      try {
        $phpFfmpeg = $container->get('php_ffmpeg');
      }
      catch (\Exception $e) {
        // Do nothing.
      }
    }
    $instance = new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('http_client'),
      $container->get('config.factory'),
      $container->get('extension.list.module'),
      $phpFfmpeg
    );
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function renderEmbedCode($width, $height, $autoplay): array {
    // @todo test with colorbox formatter.
    return [
      '#theme' => 'video_embed_html5',
      '#src' => $this->videoUrl,
      '#type' => 'video/' . $this->videoType,
      '#autoplay' => $autoplay,
      '#provider' => 'local_html5',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function downloadThumbnail(): void {
    if ($this->phpFfmpeg) {
      $local_uri = $this->getLocalThumbnailUri();
      if (!file_exists($local_uri)) {
        $this->getFileSystem()->prepareDirectory($this->thumbsDirectory, FileSystemInterface::CREATE_DIRECTORY);
        try {
          // Thumb does not exist yet. Generate it.
          $video = $this->phpFfmpeg->open($this->videoUrl);
          $video->frame(TimeCode::fromSeconds(1))
            ->save($this->getFileSystem()->realpath($this->thumbsDirectory) . '/' . $this->filename . '.jpg');
        }
        catch (\Exception $e) {
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getLocalThumbnailUri(): string {
    return $this->thumbsDirectory . '/' . $this->filename . '.jpg';
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteThumbnailUrl(): string {
    // Don't need this as we override "downloadThumbnail".
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function renderThumbnail($image_style, $link_url): array {
    $build = parent::renderThumbnail($image_style, $link_url);

    if (!file_exists($this->getLocalThumbnailUri()) && !$this->phpFfmpeg) {
      // Set uri to default placeholder.
      $uri = $this->moduleExtensionList->getPath('video_embed_html5') . '/img/placeholder.png';
      if ($this->config->get('add_placeholder')) {
        if (($file = $this->config->get('placeholder')) && ($file = File::load($file[0]))) {
          // Custom placeholder is uploaded, use this one.
          /** @var \Drupal\file\FileInterface $file */
          $uri = $file->getFileUri();
        }
      }

      // Build render array for placeholder.
      $placeholder = [
        '#theme' => 'image',
        '#uri' => $uri,
      ];

      // Generate thumb in JS and render it as canvas.
      if ($link_url) {
        $build['#title'] = [
          '#type' => 'container',
          '#attributes' => [
            'data-render-thumbnail' => $this->videoUrl,
            'id' => 'video-embed-html5-' . uniqid(),
          ],
        ];

        if ($this->config->get('add_placeholder')) {
          $build['#title']['image'] = $placeholder;
        }
      }
      else {
        $build = [
          '#type' => 'container',
          '#attributes' => [
            'data-render-thumbnail' => $this->videoUrl,
            'id' => 'video-embed-html5-' . uniqid(),
          ],
        ];

        if ($this->config->get('add_placeholder')) {
          $build['image'] = $placeholder;
        }
      }

      // Attach lib to generate thumbnails.
      $build['#attached']['library'][] = 'video_embed_html5/thumbnails';

    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public static function getIdFromInput($input) {
    $pattern = "/(?:(\/)|(?:(?|http|https|ftp)(?|\/\/))).*(mp4|ogg|webm)/i";
    $matches = [];
    preg_match($pattern, $input, $matches);

    // Make sure there are values.
    if ($matches && isset($matches[2])) {
      return [$input, $matches[2]];
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->t('@provider Video (@id)', [
        '@provider' => $this->getPluginDefinition()['title'],
        '@id' => $this->getVideoId()[0],
    ]);
  }

}
